# OpenML dataset: house_sales

https://www.openml.org/d/42092

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.csv`](./dataset/tables/data.csv): CSV file with data
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

This dataset contains house sale prices for King County, which includes Seattle. It includes homes sold between May 2014 and May 2015.

It contains 19 house features plus the price and the id columns, along with 21613 observations.
It's a great dataset for evaluating simple regression models.

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/42092) of an [OpenML dataset](https://www.openml.org/d/42092). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/42092/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/42092/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/42092/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

